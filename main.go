package main

import (
	"fmt"
	"log"
)

//UserMenu func para mostrar o menu de cadastro
func UserMenu() int {
	fmt.Println("----------MENU---------------")
	fmt.Println("| 0 - Quit                  |")
	fmt.Println("| 1 - Add User              |")
	fmt.Println("| 2 - User List             |")
	fmt.Println("| 3 - Search User           |")
	fmt.Println("| 4 - Delete User           |")
	fmt.Println("| 5 - Check User            |")
	fmt.Println("-----------------------------")

	op := -1

	for op < 0 || op > 5 {
		fmt.Scan(&op)
		fmt.Println("-------------------------------")

		if op < 0 || op > 5 {
			fmt.Println("Opção inválida!")
		}
	}

	return op
}

func main() {
	var users Users
	err := readDB(&users)
	if err != nil {
		log.Fatalln(err.Error())
	}

	for true {
		op := UserMenu()

		if op == 0 {
			break
		} else if op == 1 {
			users.AddUser()
		} else if op == 2 {
			users.UserList()
		} else if op == 3 {
			users.SearchUser()
		} else if op == 4 {
			users.DeleteUser()
		} else if op == 5 {
			fmt.Println("e-mail: ")
			email := readString()
			fmt.Println("Password: ")
			senha := readString()
			fmt.Println()
			if usuario, success := users.login(email, senha); success {
				usuario.print()
			}
		}

		err = users.saveDB()
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

}
