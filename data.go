package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

//Users vetor de funcionarios
type Users struct {
	Users    []User `json:"users"`
	FileName string
}

func readDB(users *Users) error {
	_, err := os.Stat("crud.json")
	if err != nil {
		if os.IsNotExist(err) {
			_, err := os.Create("crud.json")
			if err != nil {
				return err
			}
			err = users.saveDB()
			if err != nil {
				return err
			}
		}
	}

	f, err := os.OpenFile("crud.json", os.O_RDWR, 0755)
	defer f.Close()
	if err != nil {
		return err
	}

	err = json.NewDecoder(f).Decode(users)
	return err
}

func (users *Users) saveDB() error {
	f, err := os.OpenFile("crud.json", os.O_RDWR|os.O_CREATE, 0755)
	defer f.Close()
	if err != nil {
		return err
	}

	err = json.NewEncoder(f).Encode(users)
	return err
}

func (users *Users) login(email, senha string) (usuario *User, success bool) {
	for _, user := range users.Users {
		err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(senha))
		if err == nil {
			return &user, (err == nil)
		}
	}
	fmt.Println("Usuário não encontrado")
	return nil, false
}

//AddUser func para adicionar um usuario
func (users *Users) AddUser() {
	user := User{}

	fmt.Println("----------Add User----------")
	fmt.Println("Name: ")
	user.Name = readString()
	fmt.Println("CPF: ")
	user.CPF = readString()
	fmt.Println("e-mail: ")
	user.Email = readString()

	fmt.Println("Password: ")
	password := readString()
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		fmt.Println("Não foi possível adicionar o usuário")
		fmt.Println(err.Error())
		return
	}
	user.Password = string(bytes)

	users.Users = append(users.Users, user)

	fmt.Println("User salvo com sucesso!")
	fmt.Println("--------------------------------")
}

//UserList func para listar os usuarios
func (users *Users) UserList() {
	fmt.Println("----------User List----------")

	if len(users.Users) == 0 {
		fmt.Println("Empty...")
		fmt.Println("-------------------------------")
		return
	}

	for index, user := range users.Users {
		fmt.Printf("Index: %v\n", index)
		fmt.Printf("Name: %v\n", user.Name)
		fmt.Println("-------------------------------")
	}
}

//SearchUser func para procurar um user pelo nome
func (users *Users) SearchUser() {
	fmt.Println("----------Search Name----------")
	fmt.Print("Name: ")
	name := strings.ToLower(readString())
	fmt.Println("-------------------------------")

	for index, user := range users.Users {
		if strings.Contains(strings.ToLower(user.Name), name) {
			fmt.Printf("Index: %v\n", index)
			fmt.Printf("Name: %v\n", user.Name)
			fmt.Printf("CPF: %v\n", user.CPF)
			fmt.Printf("e-mail: %v\n", user.Email)
			fmt.Println("-------------------------------")
		}
	}
}

//DeleteUser func para deletar contato
func (users *Users) DeleteUser() {
	fmt.Println("----------Remove User-----------")
	fmt.Print("Index: ")
	var index int
	fmt.Scan(&index)

	if index >= 0 && index < len(users.Users) {
		users.Users = append(users.Users[:index], users.Users[index+1:]...)
		fmt.Println("Apagado com sucesso!")
	} else {
		fmt.Println("Índice não encontrado!")
	}
	fmt.Println("----------------------------")
}
