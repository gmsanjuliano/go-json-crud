package main

import (
	"bufio"
	"os"
	"strings"
)

func readString() string {
	scanner := bufio.NewReader(os.Stdin)
	line, _ := scanner.ReadString('\n')
	line = strings.Trim(line, "\n")

	return line
}
