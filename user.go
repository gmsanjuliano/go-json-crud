package main

import "fmt"

//User struct do user
type User struct {
	CPF      string `json:"cpf"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (user *User) print() {
	fmt.Println(user.Name)
	fmt.Println(user.CPF)
	fmt.Println(user.Email)
}
